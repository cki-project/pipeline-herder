"""Various matchers."""
import re

from . import utils

MATCHERS = [
    utils.Matcher(
        name='code137',
        description='Pod resource exhaustion (exit code 137)',
        messages=['ERROR: Job failed: command terminated with exit code 137',
                  'ERROR: Job failed: exit code 137',
                  # Temporarily disable this matcher as it's generating false positives.
                  # It matches against DW issues with the same string in 'check-kernel-results'
                  # 'No space left on device',
                  'Disk quota exceeded']
    ),
    utils.Matcher(
        name='code143',
        description='Pod killed by SIGTERM (exit code 143)',
        messages=['ERROR: Job failed: command terminated with exit code 143']
    ),
    utils.Matcher(
        name='code255',
        description='Job killed for unknown reason (exit code 255)',
        messages=['ERROR: Job failed: exit code 255']
    ),
    utils.Matcher(
        name='image-pull',
        description='Failure during image pull',
        messages=[
            'ERROR: Job failed: image pull failed',
            'ERROR: Job failed: failed to pull image',
        ],
    ),
    utils.Matcher(
        name='system-failure',
        description='System failure',
        messages='ERROR: Job failed (system failure)',
    ),
    utils.Matcher(
        name='ceph-s3',
        description='Problem accessing Ceph S3 buckets',
        messages=[
            re.compile(
                r'(download|upload) failed.*s3://' +
                r'(cki|DH-PROD-CKI|DH-SECURE-CKI)' +
                r'.*(Connection was closed before we received a valid' +
                r' response|AccessDenied|Bad Request)'),
            re.compile(
                r'Connection was closed before we received a valid ' +
                r'response from endpoint URL: ' +
                r'"https://s3.upshift.redhat.com/'),
        ]
    ),
    utils.Matcher(
        name='network-dns',
        description='Network troubles - DNS',
        messages=[
            'Name or service not known',
            'Could not resolve host'
        ]
    ),
    utils.Matcher(
        name='aws-credentials',
        description='AWS credential configuration missing',
        messages=['Unable to locate credentials. You can configure credentials'
                  ' by running "aws configure".']
    ),
    utils.Matcher(
        name='artifacts-error',
        description='Problem while uploading/downloading artifacts',
        messages=[
            re.compile(r'ERROR: Uploading artifacts.*FATAL: invalid argument', re.DOTALL),
            re.compile(r'WARNING: Downloading artifacts.*failed.*502 Bad Gateway'),
            re.compile(r'ERROR: Downloading artifacts from coordinator.*'
                       r'(forbidden.*403 Forbidden|unauthorized.*401 Unauthorized)'),
        ]
    ),
    utils.Matcher(
        name='network-gitlab-com',
        description='Network troubles reaching gitlab.com',
        messages=re.compile(
            r'ERROR: Command errored out with exit status 128: git clone.*https://gitlab.com')
    ),
    utils.Matcher(
        name='network-generic',
        description='Generic network trouble',
        messages=[
            'TLS handshake timeout',
            ('curl: (56) OpenSSL SSL_read: error:1408F119:SSL routines:ssl3_get_record:'
             'decryption failed or bad record mac, errno 0'),
        ],
    ),
    utils.TimeoutMatcher(),
    utils.Matcher(
        name='skewed-time',
        description='Node has wrong time set',
        messages=['An error occurred (RequestTimeTooSkewed) when calling']
    ),
    utils.Matcher(
        name='process-limit',
        description='Unable to fork',
        job_name='build',
        file_name='artifacts/build.log',
        tail_lines=500,
        messages='Resource temporarily unavailable',
    ),
    utils.Matcher(
        name='datawarehouse-down',
        description='connection to DataWarehouse was refused',
        job_name='check-kernel-results',
        messages=[
            'requests.exceptions.HTTPError: 404 Client Error: Not Found for url: '
            'https://datawarehouse.internal.cki-project.org/api/1/kcidb',
            'Failed to establish a new connection: [Errno 110] Connection timed out',
        ],
    ),
    utils.Matcher(
        name='bad-brew-data',
        description='Inconsistent Brew data',
        job_name='createrepo',
        messages=re.compile(r"KeyError: 'Finished'")
    ),
    utils.Matcher(
        name='missing-git-cache',
        description='Missing git cache (manual intervention needed!)',
        messages=re.compile(
            r'download failed: s3://cki/git-cache/.* error occurred '
            r'\(NoSuchKey\) when calling the GetObject operation'),
        action='report',
    ),
    utils.Matcher(
        name='force-push',
        description='Can not checkout the remote reference',
        messages=re.compile(
            r'fatal: reference is not a tree:'),
        action='report',
    ),
    utils.Matcher(
        name='too-large',
        description='Artifacts size exceeded',
        messages=re.compile(
            r'ERROR: Uploading artifacts( as "archive")? to coordinator... too large'),
        action='report',
    ),
    utils.Matcher(
        name='job-timeout',
        description='Job timeout hit, execution took longer than expected (manual check needed!)',
        messages=re.compile(r'ERROR: Job failed: execution took longer than \w+ seconds'),
        action='report',
    ),
    utils.Matcher(
        name='old-git',
        description='Old git versions cannot handle URLs without .git suffix'
                    '(manual intervention needed!)',
        messages=re.compile(
            r'error: RPC failed; result=22, HTTP code = 422.*'
            r'fatal: The remote end hung up unexpectedly'),
        action='report',
    ),
    utils.Matcher(
        name='new-buildrequires',
        description='Kernel build dependencies changed, container images need to be updated!'
                    ' (manual intervention needed!)',
        messages='error: Failed build dependencies',
        action='report',
        always_notify=True,
    ),
    utils.Matcher(  # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27022
        name='volume-cleanup-failed',
        description='gitlab-runner failed cleaning up files, might bleed into future jobs',
        messages='Failed to cleanup volumes',
        job_status=('failed', 'success'),
        action='report',
        always_notify=True,
    ),
    utils.Matcher(
        name='inotify-limit-reached',
        description='inotify_init failed, might need sysctl limit increase',
        messages=['inotify instance limit reached', 'inotify watch limit reached'],
        tail_lines=10000,
        job_name='test',
        job_status=('failed', 'success'),
        action='report',
        always_notify=True,
    ),
    utils.Matcher(
        name='upt-missing-compose',
        description='UPT couldn\'t submit a job due to a missing compose',
        messages='No distro tree matches Recipe:',
        job_status=('failed', 'success'),
        action='report',
        always_notify=True,
    ),
    utils.Matcher(
        name='upt-logged-error',
        description='UPT said it could not submit a job',
        messages='[ERROR] - cki.upt.logger - submitting job failed!',
        action='report',
        always_notify=True,
    ),
    utils.Matcher(
        # Keep an eye on how often this happens and if we need to increase the
        # retry count or need to add automatic retries.
        name='upt-provisioning-failed',
        description='UPT provisioning failed and ran out of retries',
        messages='Failed to provision resource(s) !!! No testing will be done.',
        job_status=('failed', 'success'),
        action='report',
        always_notify=True,
    ),
    utils.Matcher(
        name='beaker-down',
        description='connection to Beaker was refused',
        job_name='test',
        messages=[
            "ConnectionError: HTTPSConnectionPool(host='beaker.engineering.redhat.com'",
            'WARNING XML-RPC connection to beaker.engineering.redhat.com failed'
        ],
        action='report',
    ),
    utils.TestsNotRun(),

    # Keep it as a fallback to avoid masking other problems, just in case.
    utils.NoTraceMatcher(),
]


def match(job):
    """Compare a job against the list of matchers and return the first match."""
    return next((m for m in MATCHERS if m.check(job)), None)
