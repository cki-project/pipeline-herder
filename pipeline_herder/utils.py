"""Various utils used across the pipeline herder."""
import dataclasses
import enum
from functools import cached_property
from functools import lru_cache
import json
import pathlib
import re
from tempfile import NamedTemporaryFile
import typing

from cki_lib import gitlab
from cki_lib import misc
from cki_lib.kcidb.file import KCIDBFile
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import dateutil.parser

from . import settings

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


class RetryReason(enum.Enum):
    """Reason why a retry should (not) be performed."""

    def __init__(self, message: str, notify_irc: bool = False, retry: bool = False) -> None:
        """Create a new reason."""
        self.message = message
        self.notify_irc = notify_irc
        self.retry = retry

    RETRY_OK = ('everything fine', True, True)
    RETRY_LIMIT_REACHED = ('retry limit reached', True)
    RETRIGGERED = 'retriggered job'
    EXTERNALLY_RESTARTED = 'job restarted externally'
    NON_PRODUCTION_ENVIRONMENT = 'non-production environment'
    HERDER_ACTION = 'not configured for retries'


@dataclasses.dataclass
class RetryRating:
    """Rating whether a retry should be performed."""

    reason: RetryReason
    details: typing.Optional[str] = None

    @property
    def message(self) -> str:
        """Return the combined message including optional details."""
        if self.details:
            return f'{self.reason.message} ({self.details})'
        return self.reason.message


class CachedJob:
    """Cache information about a GitLab job to minimize API calls."""

    def __init__(self, gitlab_host, project, job_id):
        """Initialize a cached job."""
        self.gitlab_host = gitlab_host
        self.project = project
        self.job_id = job_id
        # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
        self.artifact_file = lru_cache()(self._artifact_file)

    @cached_property
    def gl_instance(self):
        """Return the GitLab instance."""
        return gitlab.get_instance(f'https://{self.gitlab_host}')

    @cached_property
    def gl_project(self):
        """Return the GitLab project."""
        return self.gl_instance.projects.get(self.project)

    @cached_property
    def gl_pipeline(self):
        """Return the GitLab pipeline."""
        return self.gl_project.pipelines.get(self.gl_job.pipeline['id'])

    @cached_property
    def gl_pipeline_jobs(self):
        """Return all jobs of the GitLab pipeline."""
        return self.gl_pipeline.jobs.list(all=True, include_retried=True)

    @cached_property
    def variables(self):
        """Return the GitLab pipeline variables."""
        return {v.key: v.value for v in self.gl_pipeline.variables.list()}

    @cached_property
    def retriggered(self):
        """Return whether the pipeline was retriggered."""
        return self.variables.get('CKI_DEPLOYMENT_ENVIRONMENT', 'production') != 'production'

    @cached_property
    def gl_job(self):
        """Return the GitLab job."""
        return self.gl_project.jobs.get(self.job_id)

    @cached_property
    def trace(self):
        """Return the GitLab job trace."""
        return self.gl_job.trace().decode('utf8', errors='replace').split('\n')

    @cached_property
    def artifacts_meta(self) -> dict:
        """Return the artifact meta data."""
        try:
            return json.loads(self.gl_job.artifact('artifacts-meta.json'))
        # pylint: disable=broad-except
        except Exception:
            return {'mode': 'gitlab'}

    def _s3_artifact_file(self, name):
        """Return one artifact file from S3."""
        response = SESSION.get(f'{self.artifacts_meta["s3_url"]}/{name}')
        response.raise_for_status()
        return response.content

    def _gitlab_artifact_file(self, name):
        """Return one artifact file from GitLab."""
        return self.gl_job.artifact(name)

    def _artifact_file(self, name, *, split=True):
        """Return one artifact file or None/an empty list if not found."""
        try:
            if self.artifacts_meta['mode'] == 'gitlab':
                artifact = self._gitlab_artifact_file(name)
            else:
                artifact = self._s3_artifact_file(name)
        # pylint: disable=broad-except
        except Exception:
            return [] if split else None
        artifact = artifact.decode('utf-8', errors='replace')
        return artifact.split('\n') if split else artifact

    @cached_property
    def auth_user_id(self):
        """Return the user owning the GitLab connection."""
        instance = self.gl_instance
        if not hasattr(instance, 'user'):
            instance.auth()
        return instance.user.id

    def job_name_count(self):
        """Return the number of jobs in the pipeline with the same name."""
        return len([j for j in self.gl_pipeline_jobs
                    if j.name == self.gl_job.name])

    def retry_delay(self):
        """Return the number of minutes to wait before a retry."""
        delay_index = self.job_name_count() - 1
        if delay_index >= len(settings.HERDER_RETRY_DELAYS):
            return settings.HERDER_RETRY_DELAYS[-1]
        return settings.HERDER_RETRY_DELAYS[delay_index]

    def retry_rating(self) -> RetryRating:
        # pylint: disable=too-many-return-statements
        """Check whether a job can be safely retried by the herder."""
        # do not retry retriggered pipelines
        if self.retriggered:
            return RetryRating(RetryReason.RETRIGGERED)

        # there should be no newer job with the same name
        newer_jobs = [f'J{j.id}' for j in self.gl_pipeline_jobs
                      if j.name == self.gl_job.name and j.id > self.gl_job.id]
        if newer_jobs:
            return RetryRating(RetryReason.EXTERNALLY_RESTARTED,
                               details=', '.join(newer_jobs))

        # only retry up to a maximum number of times
        if self.job_name_count() > settings.HERDER_RETRY_LIMIT:
            return RetryRating(RetryReason.RETRY_LIMIT_REACHED,
                               details=str(settings.HERDER_RETRY_LIMIT))

        # only retry if the herder is configured to do so
        if settings.HERDER_ACTION != 'retry':
            return RetryRating(RetryReason.HERDER_ACTION,
                               details=settings.HERDER_ACTION)

        if not misc.is_production():
            return RetryRating(RetryReason.NON_PRODUCTION_ENVIRONMENT)

        return RetryRating(RetryReason.RETRY_OK)


class Matcher:  # pylint: disable=too-few-public-methods
    """Base class for matchers."""

    # pylint: disable=too-many-arguments,too-many-instance-attributes
    def __init__(self, name, description, messages,
                 job_name=None, job_status=('failed',), file_name=None,
                 action='retry', tail_lines=300,
                 always_notify=False):
        """Initialize the matcher."""
        self.name = name
        self.description = description
        self.job_name = job_name
        self.job_status = job_status
        self.file_name = file_name
        self.action = action
        self.messages = messages if isinstance(messages, list) else [messages]
        self.tail_lines = tail_lines
        self.always_notify = always_notify

    @staticmethod
    def _check_lines(message, lines):
        """Check lines for a match."""
        if isinstance(message, re.Pattern):
            return message.search('\n'.join(lines))
        return any(message in line for line in lines)

    def check_lines(self, lines):
        """Check lines for a match."""
        return any(self._check_lines(m, lines) for m in self.messages)

    def check(self, job: CachedJob):
        """Check status/log for a match."""
        if job.gl_job.status not in self.job_status:
            return False
        if self.job_name and not job.gl_job.name.startswith(self.job_name):
            return False
        if self.file_name:
            all_lines = job.artifact_file(self.file_name, split=True)
        else:
            all_lines = job.trace

        return self.check_lines(all_lines[-self.tail_lines:])

    @staticmethod
    def get_affected_job(job: CachedJob):
        """Return the job that needs to be restarted."""
        return job


class TimeoutMatcher:  # pylint: disable=too-few-public-methods
    """Check whether a job got stuck or hit a timeout."""

    name = 'timeout'
    description = 'Job got stuck or hit timeout'
    always_notify = False
    action = 'retry'

    def check(self, job: CachedJob):  # pylint: disable=no-self-use
        """Check status/failure reason for a match."""
        if job.gl_job.status != 'failed':
            return False
        if job.gl_job.attributes.get('failure_reason') != 'stuck_or_timeout_failure':
            return False

        return True

    @staticmethod
    def get_affected_job(job: CachedJob):
        """Return the job that needs to be restarted."""
        return job


class DependingJobsMatcher:  # pylint: disable=too-few-public-methods
    """
    Checker for depending jobs.

    When a job finishes, and the job failed before, check if another one needs
    to be retriggered.

    Useful for jobs that run when=always and need to be retried after a previous
    failing job run again.
    """

    def __init__(self, job_name, depending_job_name,
                 job_status='success', action='retry',
                 always_notify=True):
        # pylint: disable=too-many-arguments
        """
        Initialize the matcher.

        job_name: Name of the first job.
        depending_job: Name of the job that needs to be retried after the first one finished.
        job_status: Necessary status of the first job.
        action: Action to perform if the check fails.
        if_failure: This job should be considered a failure.
        """
        self.job_name = job_name
        self.job_status = job_status
        self.depending_job_name = depending_job_name
        self.action = action
        self.always_notify = always_notify

        self.name = f'{self.job_name}->{self.depending_job_name} ({self.job_status})'
        self.description = self.name

    @classmethod
    def from_list(cls, rules_list):
        """Generate DependingJobsMatcher from a list of [jobs]->dependency."""
        matchers = []
        for jobs_list, dependency in rules_list:
            if not isinstance(jobs_list, list):
                jobs_list = [jobs_list]

            for job in jobs_list:
                matchers.append(
                    cls(job, dependency)
                )
        return matchers

    def check(self, job: CachedJob):
        # pylint: disable=too-many-return-statements
        """Check the job."""
        if not self.job_name == job.gl_job.name:
            return False

        if not self.job_status == job.gl_job.status:
            return False

        # there should be at least one previous failed job
        if not any(j.name == job.gl_job.name and j.id < job.gl_job.id and j.status == 'failed'
                   for j in job.gl_pipeline_jobs):
            return False

        depending_job = self.get_affected_job(job)
        if not depending_job:
            # No depending job found, so nothing that needs to be retried
            return False

        if depending_job.gl_job.status in ['canceled', 'created', 'pending']:
            # If it has not run or was cancelled, ignore it.
            return False

        if depending_job.gl_job.status == 'running':
            # If it was created before the job finished, means it was started for a different
            # job and needs to be restarted.
            depending_created_at = dateutil.parser.parse(depending_job.gl_job.created_at)
            job_finished_at = dateutil.parser.parse(job.gl_job.finished_at)
            return job_finished_at > depending_created_at

        if depending_job.gl_job.status in ['success', 'failed']:
            # It run and finished. Needs to be retried.
            return True

        # All the interesting statuses should've been handled already
        return False

    def get_affected_job(self, job: CachedJob):
        """Return the job that needs to be restarted."""
        for gl_job in job.gl_pipeline_jobs:
            if gl_job.name == self.depending_job_name:
                return CachedJob(job.gitlab_host, job.project, gl_job.id)

        return None


class NoTraceMatcher(Matcher):
    # pylint: disable=too-few-public-methods
    """Ensure the job has a trace."""

    name = 'no-trace'
    description = 'Job has no trace'
    always_notify = False

    # Retrying these jobs won't usually work.
    action = 'report'

    def __init__(self):  # pylint: disable=super-init-not-called
        """Override Matcher init parameters."""

    def check(self, job: CachedJob):
        """
        Check that the job's trace is not empty.

        job.trace returns a string split by new lines, so when the trace
        is empty the result will be a list with an empty string.
        """
        return job.trace == ['']


class TestsNotRun:  # pylint: disable=too-few-public-methods
    """Check that all tests in a test job ran or were properly skipped."""

    name = 'tests-not-run'
    description = 'Test job has tests that did not run'
    always_notify = True

    def __init__(self, *,
                 kcidbfile_path='kcidb_all.json',
                 action='report'):
        """Initialize the matcher."""
        self.kcidbfile_path = kcidbfile_path
        self.action = action

    def check(self, job: CachedJob):
        """Check that all tests ran until completion or were skipped."""
        if not job.gl_job.name.startswith('test'):
            return False

        if not (kcidb_content := job.artifact_file(self.kcidbfile_path, split=False)):
            return False

        with NamedTemporaryFile() as tmpfile:
            pathlib.Path(tmpfile.name).write_text(kcidb_content, encoding='utf8')
            kcidbfile = KCIDBFile(tmpfile.name)

        unrun_tests = [
            test for test in kcidbfile.data.get('tests', [])
            if not test.get('status') or misc.get_nested_key(test, 'misc/forced_skip_status')
        ]

        if unrun_tests:
            self.description = f'{len(unrun_tests)} test(s) that did not run'

        return unrun_tests

    @staticmethod
    def get_affected_job(job: CachedJob):
        """Return the job that needs to be restarted."""
        return job


def notify(job: CachedJob, notification, notify_irc: bool):
    """Send a message to the logs and optionally the IRC bot."""
    with misc.only_log_exceptions():
        status = job.gl_job.status
        name = job.gl_job.name
        job_id = job.gl_job.id
        pipeline_id = job.gl_pipeline.id
        retriggered = ' (retriggered)' if job.retriggered else ''
        url = misc.shorten_url(job.gl_job.web_url)
        message = (f'🤠 P{pipeline_id} J{job_id}{retriggered} {name} {status}:'
                   f' {notification} {url}')
        LOGGER.info('%s', message)
        if notify_irc and settings.IRCBOT_URL:
            SESSION.post(settings.IRCBOT_URL, json={'message': message})
